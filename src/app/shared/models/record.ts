
import * as moment from 'moment';

let TIMEFORMAT = 'DD.MM.YYYY HH:mm';

export class Record {

    public start: string;
    public end: string;
    public email: string;

    constructor() {
    }

    deserialize(input: any): this {
        if (input) {
            Object.assign(this, input, {
                email: input.email,
                start: input.start && moment.parseZone(input.start).format(TIMEFORMAT),// && DateFormatUtils.formatDateTime(input.start),
                end: input.end && moment.parseZone(input.end).format(TIMEFORMAT),// && DateFormatUtils.formatDateTime(input.end)
            });
        }
        return this;
    }

    serialize(): {} {
        let json = Object.assign({}, this, {
            email: this.email,
            start: this.start && moment(this.start).format(TIMEFORMAT),
            end: this.end && moment(this.end).format(TIMEFORMAT),
        });
        console.log(this);
        console.log(json);
        return json;
    }
}
