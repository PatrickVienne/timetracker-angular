import { environment } from '../environments/environment';
import { Component, OnInit } from '@angular/core';
import { Record } from './shared/models/record';
import { RecordsService } from './services/records/records.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {

  title = 'timetracker-angular';
  production = environment.production;

  records: Record[] = [];

  newRecord: Record = new Record();

  emailFilter: string = null;

  constructor(
    private recordsService: RecordsService
  ) {}

  ngOnInit(): void {
  }

  public onGetByEmail(e: any): void {
    e.preventDefault();

    this.recordsService.getRecordsByEmail(this.emailFilter || null).subscribe(records => {
        this.records = records||[];
    });
  }

  public onPublishRecord(e: any): void {
    e.preventDefault();

    this.recordsService.save(this.newRecord).subscribe();
  }
}
