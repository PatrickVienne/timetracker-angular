import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/index';
import { HttpClient, HttpParams, HttpHeaders } from '@angular/common/http';
import { Record } from 'src/app/shared/models/record';
import { map } from "rxjs/operators";
import { environment } from 'src/environments/environment';
import * as moment from 'moment';
import { AppConfig } from 'src/app/app.config';

let TIMEFORMAT = 'DD.MM.YYYY HH:mm';


@Injectable({
  providedIn: 'root'
})
export class RecordsService {
  url: string;
  constructor(private http: HttpClient, private appConfig:AppConfig) {
    this.url = environment.apiUrl + '/records';
   }

  public getRecordsByEmail(email: string): Observable<Record[]> {
    const opts = { params: { email: email} };

    return this.http.get<Record[]>(this.url, opts)
      .pipe(
        map(
          (records: any) => records && records.map(r => new Record().deserialize(r)) || []));
  }

  public save(record: Record): Observable<Record> {
    let params = new HttpParams();
    params = params.append('email', record.email);
    params = params.append('start', moment(record.start).format(TIMEFORMAT));
    params = params.append('end', moment(record.end).format(TIMEFORMAT));
    return this.http.post<any>(this.url, params.toString(),  {
      headers: new HttpHeaders({
           'Content-Type':  'application/x-www-form-urlencoded  ',
         })}).pipe(
      map((record: any) => record && new Record().deserialize(record) || null));
  }

}
