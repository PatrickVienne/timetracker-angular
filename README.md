# TimetrackerAngular

docker image for frontend is on:

## build frontend image
Dockerfile is available in the root directory. it includes an nginx server to start the application, and the commands to build the frontend distribution.

docker build .

## start frontend and backend

`docker-compose up`

This will get both images started

## dynamic backend url definition:

App initializer used to set baseUrl on application load.
Could introduce setting of baseurl based on environment variable.

For more visit:
source: [kutsyk multienv angular docker](https://kutsyk.github.io/multi-env-angular-docker/)

## nginx routing

nginx reroutes all requests to localhost:80/api -> api:8080

Example:
localhost:80/api/records -> api:8080/records

This works because docker-compose starts api (java backend) and angular (frontend) in the same network, so nginx can resolve "api".


## prod deployment routing
For prod deployment, make sure to correctly set the baseUrl in angular, and set all links in the services correctly.
